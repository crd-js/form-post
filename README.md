# Form Post

Creates hidden forms to post to server.

### Dependencies

- CRD Utils
- JQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2017 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details