/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD.Form');

// Creates the namepsace for the object
CRD.Form.Post = (function() {
	"use strict";
	
	/**
	 * Post
	 * @class
	 * @param {String} action  - Form action url
	 * @param {Object} params  - Params to send as form values
	 * @param {Object} options - Object options
	 * @return {Post}
	 */
	
	function Post(action, params, options) {
		
		// Set options and events
		this.setOptions(options);
		
		/**
		 * Url to post
		 * @property {String} url - Form action
		 */
		
		this.action = action;
		
		/**
		 * Params to send as post params
		 * @property {Object} params - Form params
		 */
		
		this.params = params;
		
		/**
		 * Element to use as layout element
		 * @property {Object} element - Element
		 */
		
		this.element = null;
		
		// Initializes the object
		this.Post();
		
	}
	
	// Post prototype implements ClassUtils
	Post.prototype = jQuery.extend({
		
		/**
		 * Default options
		 * @property {Object} defaults        - Default options
		 * @property {String} defaults.method - Default request method
		 * @property {String} defaults.defer  - Form must be deferred?
		 */
		
		defaults : {
			method : 'post',
			defer  : false
		},
		
		/**
		 * Send the form request
		 * @constructor
		 * @memberof Post
		 * @public
		 */
		
		Post : function() {
			
			// Creates the form element
			this.createElement();
			
			// If the form must be sent right away
			if(this.options.defer === false) {
				
				// Send theform
				this.send();
				
			}
			
		},
		
		/**
		 * Creates the form element
		 * @method createElement
		 * @return {Post}
		 * @memberOf Post
		 * @public
		 */
		
		createElement : function() {
			
			// Creates the form element
			this.element = jQuery('<form />');
			
			// Sets the form action and methos
			this.element
				.attr({
					'action' : this.action,
					'method' : this.options.method
				});
			
			// For each defined param
			for(var field in this.params) {
				
				// Filters unwanted properties
				if(this.params.hasOwnProperty(field)) {
					
					// creates the form field
					this.addField(field, this.params[field]);
					
				}
				
			}
			
			// Appends the form to the document body
			jQuery(document.body).append(this.element);
			
			// (:
			return this;
			
		},
		
		/**
		 * Recursively creates all form fields
		 * @method addField
		 * @param {String} name  - Name of the field
		 * @param {*}      value - Value for the field
		 * @memberOf Post
		 * @public
		 */
		
		addField : function(name, value) {
			
			// Variables
			var x, max, key, field;
			
			// If the value is an array
			if(jQuery.isArray(value)) {
				
				// For each defines value
				for(x = 0, max = value.length; x < max; x++) {
					
					// Adds the field
					this.addField(name + '[]', value[x]);
					
				}
				
			}
			
			// If the value is an object
			else if(typeof value === 'object') {
				
				// For each defined value
				for(key in value) {
					
					// Filters unwanted values
					if(value.hasOwnProperty(key)) {
						
						// Adds the field
						this.addField(name + '[' + key + ']', value[key]);
						
					}
					
				}
				
			}
			
			// Strings, others
			else {
				
				// Creates the field
				field = jQuery('<input />');
				
				// Sets the field properties and value
				field
					.attr({
						'type'  : 'hidden',
						'name'  : String(name),
						'value' : String(value)
					});
				
				// Adds the field to the form element
				this.element.append(field);
				
			}
			
		},
		
		/**
		 * Sends the request
		 * @method send
		 * @memberof Post
		 * @public
		 */
		
		send : function() {
			
			// Send the form
			this.element.submit();
			
		}
		
	}, CRD.ClassUtils);
	
	// Retruns the constructor
	return Post;
	
})();